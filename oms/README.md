# [Visual OMS](https://oms-dev.tenqube.com) [1.0.0](https://badge.fury.io/rb/spree.svg) [![Test Coverage](https://api.codeclimate.com/v1/badges/8277fc2bb0b1f777084f/test_coverage)](https://codeclimate.com/github/spree/spree/test_coverage)

**Visual OMS** is a open source platform

## 개요

<a href="https://www.getvendo.com/vendo-seller-network?utm_source=spree_github">
  <img src="https://dev-oms-images.s3.ap-northeast-2.amazonaws.com/Visual+Order_icon_horizon.png" />
</a>

>
> 제휴 이커머스(네이버 or 카카오) 쇼핑몰에 오프라인 점포용 사이트를 개설하여 주문을 처리하는 SaaS 서비스


## Key Features

* **[표준상품](https://oms-dev.tenqube.com)** - 표준상품 관리
* **[상품](https://oms-dev.tenqube.com)** - 점포 상품, 스토어 상품 관리
* **[점주](https://oms-dev.tenqube.com)** - 점포 주인 관리
* **[점포](https://oms-dev.tenqube.com/)** - 물류센터별 관리되는 점포 관리
* **[커머스](https://oms-dev.tenqube.com/)** - 커머스 관리
* **[물류센터](https://oms-dev.tenqube.com)** - 사용자 관리, 매니저 관리
* **[주문](https://oms-dev.tenqube.com)** - 점포별 커머스에 수집되는 주문 목록 관리
* **[배송](https://oms-dev.tenqube.com)** - 점포별 커머스에 수집되는 배송 목록 관리
* **[클레임](https://oms-dev.tenqube.com)** - 점포별 커머스에 수집되는 클레임 목록 관리
* **[정산](https://oms-dev.tenqube.com)** - 점포별 정산 관리
* **[문의/리뷰](https://oms-dev.tenqube.com)** - 커머스에 등록된 문의/리뷰 관리
* **[통계](https://oms-dev.tenqube.com)** - 통계 분석
* **[대시보드](https://oms-dev.tenqube.com)** - 전체 정보 요약

👉 [Get more sales now!](https://oms-dev.tenqube.com)

## Documentation

* [Getting Started](https://www.notion.so/kayjlee/c22fc55b86fe41188a2fb121a119a63e)
* [Developer Documentation](https://www.notion.so/kayjlee/34ed16f7a8ca4acf943adf7b12255bc0)
* [스마트스토어 연동](https://www.notion.so/kayjlee/34ed16f7a8ca4acf943adf7b12255bc0)

## API

* [물류센터 API Reference](https://www.notion.so/kayjlee/API-dae2f4c7ff5c4c81b1e345c9178d0f62)
* [점포 API Reference](https://www.notion.so/kayjlee/API-dae2f4c7ff5c4c81b1e345c9178d0f62)

## Contributing

Visual OMS is an open source project and we love contributions in any form - pull requests, issues, feature ideas!
Please review the [Contributing Guide](https://oms-dev.tenqube.com)

## License

Visual Order is released under the [New BSD License](https://gitlab.com/kdlcdev/oms-group/oms/-/blob/main/README.md).


