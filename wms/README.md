# 표준 창고/자원관리시스템(WMS/ERP)

<p align="center"><img src="./wms-circle.png" height="400"/></p>

## - 사업개요


* 본 사업은 중소 유통에서 공급망으로 활용하고 있는 중소유통공동도매물류센터(이하 중소유통물류센터)에 지역거점 중소점포 연계형 풀필먼트 표준모델(프로세스+장비)을 구축하고 센터와 연계된 중소유통사의 편리하고 경쟁력있는 옴니채널 지원을 목표로하는 산업부의 「2021년 디지털유통인프라구축사업」의 표준장비를 구축하는 사업의 세부 장비 구축 사업중 하나의 사업

* 현재 시장의 다양한 물류센터를 관리하는 SW/HW장비들은 일반적으로 각기 다른 물류센터의 업무 환경과 프로세스로 인하여 기성품으로 제작되기가 어렵고 이로 인하여 기본 기능을 제외하면 대부분 용역발주를 통하여 구축 되는 상황 

* 이로 인하여 중소 유통에서 활용중인 중소유통물류센터와 같은 중소 유통에서 주로 활용중인 물류센터들은 새로운 기술의 도입이 높은 비용과 소수의 공급기업들로 인하여 어려운 상황 

* 따라서 이를 해결하기 위하여 기존에 창고 관리 솔루션을 보유하고 있는 기업들을 대상으로 제공되는 표준화 규격에따라 기존 솔루션을 커스터마이징하여 솔루션 공급과 구축 및 관련 기술(소스코드)을 오픈소스 형태로 공유를 하는 사업
 
* 기 구축된 표준 솔루션을 각기 다른 환경의 3군데 물류센터에 설치하고 설치시 발생되는 문제점 해결 및 기존 데이터의 이관, 클라우드 서비스 연결 등 실제 작업자들의 활용할 수 있는 환경의 구성과 세팅이 필요
 
* 결과적으로 표준화된 솔루션과 관련 기술을 시장에 공급 및 공유하여 중소 유통사들의 관련 기술의 활용을 높이고 관련 장비 사업시장의 확대를 기대할 수 있는 사업


## - 구축환경

<img src="https://img.shields.io/badge/java-007396?style=for-the-badge&logo=java&logoColor=white">
<img src="https://img.shields.io/badge/react-61DAFB?style=for-the-badge&logo=react&logoColor=black">
<img src="https://img.shields.io/badge/css-1572B6?style=for-the-badge&logo=css3&logoColor=white">
<img src="https://img.shields.io/badge/javascript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black">
<img src="https://img.shields.io/badge/amazonaws-232F3E?style=for-the-badge&logo=amazonaws&logoColor=white">
<img src="https://img.shields.io/badge/node.js-339933?style=for-the-badge&logo=Node.js&logoColor=white">
<img src="https://img.shields.io/badge/postgresql-1572B6?style=for-the-badge&logo=postgresql&logoColor=white">
<img src="https://img.shields.io/badge/mongoDB-47A248?style=for-the-badge&logo=MongoDB&logoColor=white">

> Frontend:
- [devextreme-react] - Ui/Ux web for react js
- [visual studio code] - ide code reactjs
- [Node.js] - run local react js
- GIT version 2.33.1.windows.1

> BackEnd : 
- Report tool : TIBCO Jaspersoft Studio 6.18.1
- Tool code + control DB + commit code  : JetBrains IntelliJ IDEA Ultimate 2020.1
- GIT version 2.33.1.windows.1
- Test API : POSTMAN - win64-8.12.4
- Java framework : Spring Boot : 2.5.4 
- Database : postgresql , mongoDB 
- Libraries management: apache maven : 4.0.0

> Devops:
- Terraform - infrastructure-as-code software
- K8s - automating deployment, scaling, and management of containerized applications
- Gitlab: built-in version control, issue tracking, code review, CI/CD,.. applications 
- Gitlab CI/CD - CI/CD tool in Gitlab
- Argo CD - GitOps continuous delivery tool for Kubernetes
- AWS service: 
  - EKS - run K8s on AWS
  - ECR - an AWS managed container image registry service
  - Route53 - DNS service
  - VPC - virtual network
  - RDS - Database service
  - S3 - Data storage service
  - Elasticache - inmemory data store and cache service (support  redis)
  - MSK - apache kafka
  - K8s resource:
    - ambassador - API gateway
    - cert-manager - generate ssl certificate
    - Gitlab runner - run your CI/CD jobs and send the results back to GitLab
    - sealed-secret - encrypts the secret data and it can be decrypted only on the Kubernetes cluster

## - 데모(Demo)

* 아래 사이트로 접속하시면 데모화면에 접속하실수 있습니다.
>  id : ??????
>  password : ?????

>  https://staging.admin.smdc-dev.com

## - 배포

* 배포

## - 기여

* 기여

## - 버전

* 버전

## - 개발참고

* [React](https://react.dev/) - The web framework used
* [devextreme-react](https://js.devexpress.com/) - Ui/Ux web for react js 
* [visual studio code](https://code.visualstudio.com/) - ide code reactjs
* [Node.js](https://nodejs.org/en) - run local react js
* [source tree](https://www.sourcetreeapp.com/) - Tool to Visualize and manage your repositories through Sourcetree's simple Git GUI.

* [Maven](https://maven.apache.org/) - Dependency Management
* [Report tool](https://community.jaspersoft.com/project/jaspersoft-studio/) - TIBCO Jaspersoft Studio 6.18.1
* [IntelliJ IDEA](https://www.jetbrains.com/idea/business/) - JetBrains IntelliJ IDEA Ultimate 2020.1
* [Java framework](https://www.postgresql.org/) - Spring Boot : 2.5.4 
* [Postgresql](https://spring.io/) - Database
* [mongoDB](https://www.mongodb.com/) - Database

## - 협력업체

* **KEA 한국전자정보통신산업진흥회** - *사업기획* - [kea](http://www.gokea.org/kor/)
* **주식회사 두손CNI** - *WMS개발* - [doosoun-cni](http://www.doosoun-cni.com)
* **주식회사 코텍** - *Das, Dps설비* - [kotech](http://www.e-kotech.co.kr/)
* **주식회사 솔루엠** - *ESL설비* - [soulm](https://www.solumesl.com/)

## - 저작권

* Copyright © 2023 kea, Doosoun-cni.
* 이 프로젝트는 MIT 라이선스 조건에 따라 라이선스가 부여됩니다. [LICENSE.md](LICENSE.md)

## - 홍보

* 홍보문구
