# 표준 자동발주시스템(AOS) 

<p align="center"><img src="./aos.jpg" height="400"/></p>

## - 사업개요


* 기 구축된 표준 솔루션을 각기 다른 환경의 3군데 물류센터에 설치하고, 설치시 발생하는 문제점들을 해결한다.
* 기존에 사용중인 데이터를 표준 자동발주시스템(AOS)로 이관하여, 데이터의 단절없이 운영한다.
* ERP, WMS, HUB 시스템과의 통합 연동으로 시스템에 의한 업무 효율화를 극대화한다.
* 단순, 반복되는 보고 문서 업무를 시스템화하여, 센터내 인원 운영 효율성을 높인다.


## - 구축환경

<img src="https://img.shields.io/badge/vue.js-4FC08D?style=for-the-badge&logo=vue.js&logoColor=white">
<img src="https://img.shields.io/badge/node.js-339933?style=for-the-badge&logo=Node.js&logoColor=white">
<img src="https://img.shields.io/badge/amazonaws-232F3E?style=for-the-badge&logo=amazonaws&logoColor=white">
<img src="https://img.shields.io/badge/php-007396?style=for-the-badge&logo=php&logoColor=white">
<img src="https://img.shields.io/badge/mariaDB-003545?style=for-the-badge&logo=mariaDB&logoColor=white">

> Frontend:
- Vuejs: The Progressive JavaScript Framework(Version 3.2)
- Nodejs: compile code (Version: 16.7) - npm (Version: 8.15)
- Test API: Postman

> BackEnd : 
- Laravel: PHP Framework (Version 9.2)
- Database: Mariadb Server (Version 10)
- Server run: nginx (Version 1.20)

> Tools:
- IDE code: Visual studio code
- Source Version control: Git
- Repository source code: Gitlab

## - 데모(Demo)

* 아래 사이트로 접속하시면 데모화면에 접속하실수 있습니다.
>  id : ??????
>  password : ?????

>  https://aos.app4cafe24.com/

## - 배포

* 배포

## - 기여

* 기여

## - 버전

* 기여

## - 개발참고

* [Node.js](https://nodejs.org/en) - run local react js
* [Vuejs](https://vuejs.org/) - The Progressive JavaScript Framework(Version 3.2)
* [Laravel](https://laravel.com/) - PHP Framework (Version 9.2)
* [Mariadb](https://mariadb.org/) - Mariadb Server (Version 10)
* [visual studio code](https://code.visualstudio.com/) - ide code reactjs

## - 협력업체

* **KEA 한국전자정보통신산업진흥회** - *사업기획* - [kea](http://www.gokea.org/kor/)
* **주식회사 두손CNI** - *WMS개발* - [doosoun-cni](http://www.doosoun-cni.com)

## - 저작권

* Copyright © 2023 kea, Doosoun-cni.
* 이 프로젝트는 MIT 라이선스 조건에 따라 라이선스가 부여됩니다. [LICENSE.md](LICENSE.md)

## - 홍보

* Hat tip to anyone whose code was used
* Inspiration
* etc
